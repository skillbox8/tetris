FROM node

RUN apt-get update && apt-get install -y mc vim nano

RUN mkdir /skillbox
WORKDIR /skillbox/flatris
COPY package.json /skillbox
RUN yarn install

COPY . /skillbox

RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
